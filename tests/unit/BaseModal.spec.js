import { shallowMount } from '@vue/test-utils'
import BaseModal from '@/components/BaseModal'

describe('Test for BaseModal.vue', () => {
    let wrapper = null

    beforeEach(() => {
        wrapper = shallowMount(BaseModal)
    })

    it('modal renders', () => {
        expect(wrapper.find('.modal-header').text()).toEqual('default header')
        expect(wrapper.find('.modal-body').text()).toEqual('default body')
        expect(wrapper.find('.modal-default-button').text()).toEqual('Retry')
    })

    it('emits the close event', () => {
        wrapper.vm.$emit('close')
        expect(wrapper.emitted().close).toBeTruthy()
    })
})
