import { mount, shallowMount } from '@vue/test-utils'
import App from '@/App'
import axios from 'axios'

jest.mock('axios')

global.console.log = jest.fn()

describe('Test for App.vue', () => {
    let wrapper = null

    beforeEach(() => {
        const responseGet = {
            data: {
                userId: 1,
                id: 1,
                title: "delectus aut autem",
                completed: false
            }
        }
        axios.get.mockResolvedValue(responseGet)

        wrapper = shallowMount(App, {
            data: () => ({ todoId: 1 })
        })
    })

    afterEach(() => {
        jest.resetModules()
        jest.clearAllMocks()
    })

    it('does load the todo item when GET is successful', async () => {
        await wrapper.findAll('button').at(0).trigger('click')
        expect(axios.get).toHaveBeenCalledTimes(1)
        expect(axios.get).toBeCalledWith(expect.stringMatching(/1/))

        await wrapper.vm.$nextTick()
        expect(wrapper.vm.todoData.userId).toBe(1)
        expect(wrapper.vm.todoData.id).toBe(1)
        expect(wrapper.vm.todoData.title).toMatch('delectus aut autem')
        expect(wrapper.vm.todoData.completed).toEqual(false)
    })
})

describe('Test for request timeout', () => {
    let wrapper = null

    beforeEach(() => {
        axios.get.mockRejectedValue(new Error('TIMEOUT'))
        wrapper = mount(App, {
            data: () => ({ todoId: 1 })
        })
    })

    afterEach(() => {
        jest.resetModules()
        jest.clearAllMocks()
    })

    it('display popup when timeout occur', async () => {
        await wrapper.findAll('button').at(0).trigger('click')
        expect(axios.get).toHaveBeenCalledTimes(1)
        expect(axios.get).toBeCalledWith(expect.stringMatching(/1/))

        await wrapper.vm.$nextTick()
        expect(wrapper.find('#modal').exists()).toBe(true)
        expect(wrapper.vm.errorMessage).toMatch('Sorry, network is down')
        expect(global.console.log).toHaveBeenCalledWith('TIMEOUT');
    })
})
