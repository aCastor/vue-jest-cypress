// https://docs.cypress.io/api/introduction/api.html

describe('E2E test', () => {
  it('Visit app home page and fetch todo', () => {
    cy.visit('/')
    cy.contains('h1', 'Test app')
    cy.contains('.btn', 'Fetch')

    cy.get('input').type('1')
    cy.get('.btn').click()
    cy.server()
    cy.route('GET', 'https://jsonplaceholder.typicode.com/todos/1', {
      userId: 1,
      id: 1,
      title: "delectus aut autem",
      completed: false
    })
    cy.contains('"title": "delectus aut autem",')
  })

  it('Visit app home page and emulate timeout', async () => {
    cy.visit('/')

    cy.server()
    cy.route({
      method: 'GET',
      url: 'https://jsonplaceholder.typicode.com/todos/1',
      status: 408,
      response: { message: 'whater ever' }
    }).as('getTodo')

    cy.get('input').type('1')
    cy.get('.btn').click()

    cy.contains('.modal-header', 'Server error')
    cy.contains('.modal-body', 'Sorry, network is down')
  })
})
